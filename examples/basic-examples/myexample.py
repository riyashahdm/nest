#from psycopg2 import connect
from nest.experiment import *
from nest.topology import *

red = Node('red')
blue = Node('blue')

(eth0, eth1) = connect(red, blue)

eth0.set_address('10.0.0.1/24')
eth1.set_address('10.0.0.2/24')

red.ping(eth1.address)

eth0.set_attributes('5mbit', '5ms')
eth1.set_attributes('10mbit', '100ms')

flow = Flow(red, blue, eth1.address, 0, 10, 6)
exp = Experiment('FORKED_tcp_6up_netperf')
# exp.add_tcp_flow(flow, 'reno', 'iperf3') # netperf
exp.add_tcp_flow(flow, 'cubic', "netperf")
# exp.add_udp_flow(flow, target_bandwidth="12mbit")
exp.run()

# class ToolSpecs
# tool, specifications
